const fs = require('fs');
const os = require('os');
const appRoot = require('app-root-path');

if (!['linux', 'darwin', 'win32'].includes(os.platform())) {
  throw new Error('unsupported platform');
  return;
}

const binDir = `${appRoot}/node_modules/.bin`
fs.mkdirSync(binDir, { recursive: true })

const ext = os.platform() === 'win32' ? '.exe' : '';

fs.copyFile(
  `${__dirname}/binary/${os.platform()}/unison-fsmonitor${ext}`,
  `${binDir}/unison-fsmonitor${ext}`,
  () => { }
);
fs.copyFile(
  `${__dirname}/binary/${os.platform()}/unison${ext}`,
  `${binDir}/unison${ext}`,
  () => { }
);