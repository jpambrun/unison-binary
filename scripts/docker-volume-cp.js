#!/usr/bin/env node
const exec = require('child_process').execSync;

const tempContainerName = `temp${Math.round(Math.random()*1000)}`;
const volume = process.argv[2];
const src = process.argv[3] || '.';
const dst = process.argv[4] || '/';

if (!volume) {
  console.log('USAGE: node docker-volume-cp.js [volume] [src] [dst]');
}

try {
  exec(`docker container create --name ${tempContainerName} -v ${volume}:/root grammarly/scratch`);
  exec(`docker cp ${src} ${tempContainerName}:/root/${dst}`);
} catch (err) {
  console.err(err);
} finally {
  exec(`docker rm ${tempContainerName}`);
}
